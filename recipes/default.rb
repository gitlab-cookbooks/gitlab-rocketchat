#
# Cookbook Name:: gitlab-rocketchat
# Recipe:: default
#
# Copyright 2016, GitLab Inc.
#
include_recipe 'gitlab-vault'

# Fetch secrets from Chef Vault
rocketchat_conf = GitLab::Vault.get(node, 'gitlab-rocketchat')

## Setup nginx
include_recipe 'chef_nginx'
template "#{node['nginx']['dir']}/sites-available/rocketchat" do
  source 'nginx-site-rocketchat.erb'
  owner  'root'
  group  'root'
  mode   '0644'
  variables(
    :fqdn => rocketchat_conf['fqdn']
  )
  notifies :restart, resources(:service => 'nginx')
end

nginx_site 'rocketchat' do
  enable true
end

file "/etc/ssl/#{rocketchat_conf['fqdn']}.crt" do
  content rocketchat_conf['ssl_certificate']
  owner 'root'
  group 'root'
  mode 0644
  notifies :restart, resources(:service => 'nginx'), :delayed
end

file "/etc/ssl/#{rocketchat_conf['fqdn']}.key" do
  content rocketchat_conf['ssl_key']
  owner 'root'
  group 'root'
  mode 0600
  notifies :restart, resources(:service => 'nginx'), :delayed
end

## Setup mongodb
file '/etc/apt/sources.list.d/mongodb-org-3.0.list' do
  content "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse\n"
  notifies :run, 'bash[add key]', :immediately
end

bash 'add key' do
  code <<-EOH
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
    apt-get update
  EOH
  action :nothing
end

%w(mongodb-org sysfsutils curl graphicsmagick npm build-essential supervisor).each {|pkg| package pkg }

cookbook_file '/etc/sysfs.conf' do
  mode '0644'
end

service 'mongod' do
  action [ :enable, :start ]
end

## Setup rocketchat
user 'rocketchat' do
  home '/usr/share/rocketchat'
  shell '/bin/false'
end

bash 'install node' do
  code <<-EOH
    npm install -g n
    n 0.10.40
  EOH
  not_if { File.exists?('/usr/local/bin/node') }
end

rocketchat_version = rocketchat_conf['version']

remote_file "#{Chef::Config[:file_cache_path]}/rocket.chat-#{rocketchat_version}.tgz" do
  source "https://rocket.chat/releases/#{rocketchat_version}/download"
  action :create_if_missing
end

bash 'install_rocketchat' do
  cwd Chef::Config[:file_cache_path]
  user 'root'
  code <<-EOH
    rm -rf #{rocketchat_conf['install_path']}/rocketchat
    tar zxf rocket.chat-#{rocketchat_version}.tgz
    mv bundle #{rocketchat_conf['install_path']}/rocketchat
    chown -R rocketchat:rocketchat #{rocketchat_conf['install_path']}/rocketchat
    echo '#{rocketchat_version}' > #{rocketchat_conf['install_path']}/rocketchat/VERSION
  EOH
  not_if "test `cat #{rocketchat_conf['install_path']}/rocketchat/VERSION` = #{rocketchat_version}"
  notifies :run, 'bash[install_npm_modules]'
end

bash 'install_npm_modules' do
  cwd "#{rocketchat_conf['install_path']}/rocketchat/programs/server"
  environment 'HOME' => '/usr/share/rocketchat'
  user 'rocketchat'
  group 'rocketchat'
  code <<-EOH
    npm install
  EOH
  notifies :restart, 'service[supervisor]'
  action :nothing
end

service 'supervisor' do
  action [ :enable, :start ]
end

template "/etc/supervisor/conf.d/rocketchat.conf" do
  mode 0644
  variables(
    :install_path => rocketchat_conf['install_path'],
    :fqdn => rocketchat_conf['fqdn']
  )
  notifies :restart, 'service[supervisor]'
end
