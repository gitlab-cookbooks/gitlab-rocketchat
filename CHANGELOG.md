gitlab-rocketchat CHANGELOG
========================

This file is used to list changes made in each version of the gitlab-rocketchat cookbook.

0.1.1
-----
- Jeroen - Use gitlab-vault and chef_nginx

0.1.0
-----
- Jeroen - Initial release of gitlab-rocketchat

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
