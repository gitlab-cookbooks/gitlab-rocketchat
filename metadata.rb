name             'gitlab-rocketchat'
maintainer       'GitLab Inc.'
maintainer_email 'jeroen@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-rocketchat'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'

depends 'gitlab-vault'
depends 'chef_nginx'
