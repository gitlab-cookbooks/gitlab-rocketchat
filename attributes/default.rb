default['gitlab-rocketchat']['version'] = '0.21.0'
default['gitlab-rocketchat']['fqdn'] = node['fqdn']
default['gitlab-rocketchat']['install_path'] = '/usr/share'
default['gitlab-rocketchat']['ssl_certificate'] = 'override_attribute in role!'
default['gitlab-rocketchat']['ssl_key'] = 'override_attribute in vault!'

default['nginx']['default_site_enabled'] = false
